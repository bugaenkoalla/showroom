package com.car_showroom.car.entity;

import com.car_showroom.engine.entity.Engine;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "car")
public class Car {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", columnDefinition = "VARBINARY(36)")
    private UUID id;
    @Column(name = "price")
    private double price;
    @Column(name = "model")
    private String model;
    @Column(name = "year")
    private int year;
    @Column(name = "color")
    private String color;

//    @Column(name = "engine_id")
//    private UUID engineId;
//    @Column(name = "person_id")
//    private UUID personId;
//    @Column(name = "showroom_id")
//    private UUID showroomId;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "engine_id", referencedColumnName = "id")
    private Engine engine;

//    @ManyToOne
//    @JoinColumn(name = "person_id", referencedColumnName = "id", insertable = false, updatable = false)
//    private Person person;

    //    @ManyToOne
//    @JoinColumn(name = "showroom_id", referencedColumnName = "id", insertable = false, updatable = false)
//    private Showroom showroom;
    public Car() {
    }

    public Car(double price, String model, int year, String color) {
        this.price = price;
        this.model = model;
        this.year = year;
        this.color = color;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

//    public UUID getEngineId() {
//        return engineId;
//    }
//
//    public void setEngineId(UUID engineId) {
//        this.engineId = engineId;
//    }

}

