package com.car_showroom.car.service;


import com.car_showroom.car.entity.Car;
import com.car_showroom.car.repository.CarRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CarService {

    private final CarRepository carRepository;

    @Autowired
    public CarService(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    public List<Car> getAllCars() {
        return carRepository.findAll();
    }

    public Car saveNewCar(Car car) {
        return carRepository.save(car);
    }

    public List<Car> getCarByModel(String model) {
        return carRepository.findByModel(model);
    }

    public Car getCarById(UUID id) {
        return carRepository.findCarByIdViaSQL(id);
    }

    public void delete(UUID id) {
        carRepository.deleteById(id);
    }

    public List<Car> displayCarUsingFilters() {
        return carRepository.findAll().stream()
                .filter(car -> car.getColor().equals("Red") && car.getModel().equals("Audi"))
                .sorted(Comparator.comparing(Car::getPrice).reversed())
                .limit(5)
                .collect(Collectors.toList());
    }
}