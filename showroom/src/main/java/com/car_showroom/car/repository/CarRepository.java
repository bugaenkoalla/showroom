package com.car_showroom.car.repository;

import com.car_showroom.car.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface CarRepository extends JpaRepository<Car, UUID>, CrudRepository<Car, UUID> {
    List<Car> findAll();

    Optional<Car> findById(UUID id);

    @Query(value = "SELECT * FROM car_showroom.car WHERE id=:id", nativeQuery = true)
    Car findCarByIdViaSQL(@Param("id") UUID id);

    List<Car> findByModel(String model);

}




