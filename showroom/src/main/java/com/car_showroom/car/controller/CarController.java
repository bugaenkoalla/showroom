package com.car_showroom.car.controller;

import com.car_showroom.car.entity.Car;
import com.car_showroom.car.service.CarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class CarController {
    private final CarService carService;

    @Autowired
    public CarController(CarService carService) {
        this.carService = carService;
    }

    @GetMapping("/cars")
    public List<Car> displayAllCars() {
        List<Car> carList = carService.getAllCars();
        return carList;
    }

    @PostMapping("/cars/add_new_car")
    public Car addNewCar(@RequestBody Car car) {
        carService.saveNewCar(car);
        return car;
    }

    @GetMapping("/cars/find_by_model/{model}")
    public List<Car> displayCarByModel(@PathVariable(value = "model") String model) {
        List<Car> carByModel = carService.getCarByModel(model);
        return carByModel;
    }

    @GetMapping("/cars/find_by_id/{id}")
    public Car displayCarById(@PathVariable(value = "id") UUID id) {
        Car carById = carService.getCarById(id);
        return carById;
    }

    @DeleteMapping("/cars/{id}/delete")
    public void deleteCar(@PathVariable(value = "id") UUID id) {
        carService.delete(id);
    }

    @GetMapping("/cars/show_5_expensive_red_audi")
    public List<Car> displayCarUsingFilters() {
        List<Car> listOfFilteredCars = carService.displayCarUsingFilters();
        return listOfFilteredCars;
    }
}

