package com.car_showroom.person.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name = "person")
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", columnDefinition = "VARBINARY(36)")
    private UUID id;
    @Column(name = "email")
    private String email;
    @Column(name = "username")
    private String username;
    @Column(name = "phone")
    private String phone;
//    @Column(name = "showroom_id")
//    private UUID showroomId;

//    @OneToMany(mappedBy = "person")
//    private List<Car> cars;

//    @ManyToOne
//    @JoinColumn(name = "showroom_id", referencedColumnName = "id", insertable = false, updatable = false)
//    private Showroom showroom;
    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "person_id")
    private List<Person> personList;

    public Person() {

    }

    public Person(String email, String username, String phone) {
        this.email = email;
        this.username = username;
        this.phone = phone;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public UUID getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getUsername() {
        return username;
    }

    public String getPhone() {
        return phone;
    }

    public List<Person> getPersonList() {
        return personList;
    }
}