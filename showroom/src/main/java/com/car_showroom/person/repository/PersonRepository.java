package com.car_showroom.person.repository;

import com.car_showroom.person.entity.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface PersonRepository extends JpaRepository<Person, UUID>, CrudRepository<Person, UUID> {
    List<Person> findAll();

    Optional<Person> findById(UUID id);

    @Query(value = "SELECT * FROM car_showroom.person WHERE id=:id", nativeQuery = true)
    Person findPersonByIdViaSQL(@Param("id") UUID id);

    List<Person> findByUsername(String username);
}




