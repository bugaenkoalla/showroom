package com.car_showroom.person.controller;

import com.car_showroom.person.entity.Person;
import com.car_showroom.person.service.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class PersonController {
    private final PersonService personService;

    @Autowired
    public PersonController(PersonService personService) {
        this.personService = personService;
    }

    @GetMapping("/persons")
    public List<Person> displayAllPersons() {
        List<Person> personList = personService.getAllPersons();
        return personList;
    }

    @PostMapping("/persons/add_new_person")
    public Person addNewPerson(@RequestBody Person person) {
        personService.saveNewPerson(person);
        return person;
    }

    @GetMapping("/persons/find_by_username/{username}")
    public List<Person> displayPersonByUsername(@PathVariable(value = "username") String username) {
        List<Person> personByUsername = personService.getPersonByUsername(username);
        return personByUsername;
    }

    @GetMapping("/persons/find_by_id/{id}")
    public Person displayPersonById(@PathVariable(value = "id") UUID id) {
        Person personById = personService.getPersonById(id);
        return personById;
    }

    @DeleteMapping("/persons/{id}/delete")
    public void deletePerson(@PathVariable(value = "id") UUID id) {
        personService.delete(id);
    }
}

