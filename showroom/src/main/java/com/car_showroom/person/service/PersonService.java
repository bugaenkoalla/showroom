package com.car_showroom.person.service;


import com.car_showroom.person.entity.Person;
import com.car_showroom.person.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class PersonService {

    private final PersonRepository personRepository;

    @Autowired
    public PersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    public List<Person> getAllPersons() {
        return personRepository.findAll();
    }

    public Person saveNewPerson(Person person) {
        return personRepository.save(person);
    }

    public List<Person> getPersonByUsername(String username) {
        return personRepository.findByUsername(username);
    }

//    public Car getCarById(UUID id) {
//        String idString = id.toString();
//        return carRepository.findCarByIdViaSQL(idString);
//    }

    public Person getPersonById(UUID id) {
        return personRepository.findPersonByIdViaSQL(id);
    }

    public void delete(UUID id) {
        personRepository.deleteById(id);
    }
}