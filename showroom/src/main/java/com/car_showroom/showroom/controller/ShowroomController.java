package com.car_showroom.showroom.controller;

import com.car_showroom.showroom.entity.Showroom;
import com.car_showroom.showroom.service.ShowroomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class ShowroomController {
    private final ShowroomService showroomService;

    @Autowired
    public ShowroomController(ShowroomService showroomService) {
        this.showroomService = showroomService;
    }

    @GetMapping("/showrooms")
    public List<Showroom> displayAllShowrooms() {
        List<Showroom> showroomList = showroomService.getAllShowrooms();
        return showroomList;
    }

    @PostMapping("/showrooms/add_new_showroom")
    public Showroom addNewShowroom(@RequestBody Showroom showroom) {
        showroomService.saveNewShowroom(showroom);
        return showroom;
    }

    @GetMapping("/showrooms/find_by_location/{location}")
    public List<Showroom> displayShowroomByUsername(@PathVariable(value = "location") String location) {
        List<Showroom> personByUsername = showroomService.getShowroomByUsername(location);
        return personByUsername;
    }

    @GetMapping("/showrooms/find_by_id/{id}")
    public Showroom displayShowroomById(@PathVariable(value = "id") UUID id) {
        Showroom showroomById = showroomService.getShowroomById(id);
        return showroomById;
    }

    @DeleteMapping("/showrooms/{id}/delete")
    public void deletePerson(@PathVariable(value = "id") UUID id) {
        showroomService.delete(id);
    }
}

