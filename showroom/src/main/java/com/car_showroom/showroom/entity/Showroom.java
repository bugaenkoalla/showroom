package com.car_showroom.showroom.entity;


import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Table(name = "showroom")
public class Showroom {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", columnDefinition = "VARBINARY(36)")
    private UUID id;
    @Column(name = "location")
    private String location;

//    @OneToMany(mappedBy = "showroomId")
//    private List<Person> person;
//
//
//    @OneToMany(mappedBy = "showroomId")
//    private List<Car> car;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "showroom_id")
    private List<Showroom> personList;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "showroom_id")
    private List<Showroom> carsList;

    public Showroom() {
    }

    public Showroom(String location) {
        this.location = location;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public UUID getId() {
        return id;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}

