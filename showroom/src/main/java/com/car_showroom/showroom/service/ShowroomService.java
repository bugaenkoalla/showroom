package com.car_showroom.showroom.service;


import com.car_showroom.showroom.entity.Showroom;
import com.car_showroom.showroom.repository.ShowroomRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class ShowroomService {

    private final ShowroomRepository showroomRepository;

    @Autowired
    public ShowroomService(ShowroomRepository showroomRepository) {
        this.showroomRepository = showroomRepository;
    }

//    public List<Car> getCarsWithSomeCriteria() {
//        List<Car> allUsers = carRepository.findAll();
//
//        // Apply additional processing using Stream API or other logic
//        return allUsers.stream()
//                .filter(car -> /* some condition */)
//                .collect(Collectors.toList());
//    }

    public List<Showroom> getAllShowrooms() {
        return showroomRepository.findAll();
    }

    public Showroom saveNewShowroom(Showroom showroom) {
        return showroomRepository.save(showroom);
    }

    public List<Showroom> getShowroomByUsername(String location) {
        return showroomRepository.findByLocation(location);
    }

//    public Car getCarById(UUID id) {
//        String idString = id.toString();
//        return carRepository.findCarByIdViaSQL(idString);
//    }

        public Showroom getShowroomById(UUID id) {
        return showroomRepository.findShowroomByIdViaSQL(id);
    }

    public void delete(UUID id) {
        showroomRepository.deleteById(id);
    }
}