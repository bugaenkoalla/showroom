package com.car_showroom.showroom.repository;

import com.car_showroom.showroom.entity.Showroom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ShowroomRepository extends JpaRepository<Showroom, UUID>, CrudRepository<Showroom, UUID> {
    List<Showroom> findAll();

    Optional<Showroom> findById(UUID id);

    List<Showroom> findByLocation(String location);

    @Query(value = "SELECT * FROM showroom WHERE id=:showroomId", nativeQuery = true)
    Showroom findShowroomByIdViaSQL(@Param("showroomId") UUID showroomId);
}



