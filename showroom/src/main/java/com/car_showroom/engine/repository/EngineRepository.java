package com.car_showroom.engine.repository;

import com.car_showroom.engine.entity.Engine;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface EngineRepository extends JpaRepository<Engine, UUID>, CrudRepository<Engine, UUID> {
    List<Engine> findAll();

    Optional<Engine> findById(UUID id);

    Engine findByVinCode(String vinCode);

    @Query(value = "SELECT * FROM car_showroom.engine WHERE id=:id", nativeQuery = true)
    Engine findEngineByIdViaSQL(UUID id);
}



