package com.car_showroom.engine.controller;

import com.car_showroom.engine.entity.Engine;
import com.car_showroom.engine.service.EngineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.UUID;

@RestController
public class EngineController {
    private final EngineService engineService;

    @Autowired
    public EngineController(EngineService engineService) {
        this.engineService = engineService;
    }

    @GetMapping("/engines")
    public List<Engine> displayAllEngines() {
        List<Engine> engineList = engineService.getAllEngines();
        return engineList;
    }

    @PostMapping("/engines/add_new_engine")
    public Engine addNewEngine(@RequestBody Engine engine) {
        engineService.saveNewEngine(engine);
        return engine;
    }

    @GetMapping("/engines/find_by_VIN_code/{VIN_code}")
    public Engine displayEngineByVinCode(@PathVariable(value = "VIN_code") String vinCode) {
        Engine engineByVinCode = engineService.getEngineByVinCode(vinCode);
        return engineByVinCode;
    }

    @GetMapping("/engines/find_by_id/{id}")
    public Engine displayCarById(@PathVariable(value = "id") UUID id) {
        Engine engineById = engineService.getEngineById(id);
        return engineById;
    }

    @DeleteMapping("/engines/{id}/delete")
    public void deleteEngine(@PathVariable(value = "id") UUID id) {
        engineService.delete(id);
    }
}

