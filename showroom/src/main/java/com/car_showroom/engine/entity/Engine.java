package com.car_showroom.engine.entity;

import com.car_showroom.car.entity.Car;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.util.UUID;

@Entity
@Table(name = "engine")
public class Engine {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", columnDefinition = "VARBINARY(36)")
    private UUID id;
    @Column(name = "power")
    private String power;
    @Column(name = "capacity")
    private String capacity;
    @Column(name = "vin_code", unique = true)
    private String vinCode;

    @OneToOne(mappedBy = "engine")
    private Car car;

    public Engine() {
    }

    public Engine(String power, String capacity, String vinCode) {
        this.power = power;
        this.capacity = capacity;
        this.vinCode = vinCode;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }


    public String getPower() {
        return power;
    }

    public void setPower(String power) {
        this.power = power;
    }

    public String getCapacity() {
        return capacity;
    }

    public void setCapacity(String capacity) {
        this.capacity = capacity;
    }

    public String getVinCode() {
        return vinCode;
    }

    public void setVinCode(String vinCode) {
        this.vinCode = vinCode;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }
}

