package com.car_showroom.engine.service;


import com.car_showroom.engine.entity.Engine;
import com.car_showroom.engine.repository.EngineRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class EngineService {

    private final EngineRepository engineRepository;

    @Autowired
    public EngineService(EngineRepository engineRepository) {
        this.engineRepository = engineRepository;
    }

    public List<Engine> getAllEngines() {
        return engineRepository.findAll();
    }

    public Engine saveNewEngine(Engine engine) {
        return engineRepository.save(engine);
    }

    public Engine getEngineByVinCode(String vinCode) {
        return engineRepository.findByVinCode(vinCode);
    }


    public Engine getEngineById(UUID id) {
        return engineRepository.findEngineByIdViaSQL(id);
    }

    public void delete(UUID id) {
        engineRepository.deleteById(id);
    }
}