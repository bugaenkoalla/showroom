package com.car_showroom.car.repositoryTest;


import com.car_showroom.car.entity.Car;
import com.car_showroom.car.repository.CarRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
class CarRepositoryTest {

    @Autowired
    private CarRepository carRepository;
    private static final String COLOR = "Red" + new Random().nextInt(5);
    private static final String UPDATED_COLOR = "Black" + new Random().nextInt(5);
    private static final String MODEL = "Audi" + new Random().nextInt(5);
    private static final double PRICE = 10.25;
    private static final int YEAR = 2020;
    private static final UUID SHOWROOM_ID = UUID.randomUUID();
    private static final UUID PERSON_ID = UUID.randomUUID();
    String newColor;
    String newModel;
    UUID newId;
    double newPrice;
    int newYear;
    UUID newPersonId;
    UUID newShowroomId;
    Car newCarFromDB;

    @BeforeEach
    public void setUp() {
        Car newCar = new Car(PRICE, MODEL, YEAR, COLOR);
        carRepository.save(newCar);
        newColor = newCar.getColor();
        newModel = newCar.getModel();
        newPrice = newCar.getPrice();
        newYear = newCar.getYear();
        newId = newCar.getId();
        newCarFromDB = carRepository.findById(newId).get();
    }


    @AfterEach
    public void performPostConditions() {
        if (carRepository.findById(newId).isEmpty()) {
            System.out.println("Nothing to delete");
        } else {
            carRepository.deleteById(newId);
        }
    }

    @Test
    void saveCarTest() {
        Assertions.assertEquals(COLOR, carRepository.findById(newId).get().getColor());
        Assertions.assertEquals(MODEL, carRepository.findById(newId).get().getModel());
        Assertions.assertEquals(PRICE, carRepository.findById(newId).get().getPrice());
        Assertions.assertEquals(YEAR, carRepository.findById(newId).get().getYear());
    }

    @Test
    void getCarTest() {
        newCarFromDB = carRepository.findById(newId).get();
        Assertions.assertEquals(newCarFromDB.getId(), newId);
    }

    @Test
    void getListOfCarTest() {
        List<Car> carList = carRepository.findAll();
        assertThat(carList.size()).isGreaterThan(0);
    }

    @Test
    void updateCarTest() {
        newCarFromDB.setColor(UPDATED_COLOR);
        Car carUpdated = carRepository.save(newCarFromDB);
        assertThat(carUpdated.getColor()).isEqualTo(UPDATED_COLOR);
    }

    @Test
    void deleteCarTest() {
        //carRepository.delete(newCarFromDB);
        carRepository.deleteById(newId);
        Car deletedCar = carRepository.findById(newId).orElse(null);
        Assertions.assertNull(deletedCar);
    }
}

