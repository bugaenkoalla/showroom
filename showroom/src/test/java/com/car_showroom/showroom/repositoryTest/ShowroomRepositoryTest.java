package com.car_showroom.showroom.repositoryTest;


import com.car_showroom.showroom.entity.Showroom;
import com.car_showroom.showroom.repository.ShowroomRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
class ShowroomRepositoryTest {

    @Autowired
    private ShowroomRepository showroomRepository;
    private static final String LOCATION = "London" + new Random().nextInt(1000);
    private static final String UPDATED_LOCATION = "Paris" + new Random().nextInt(1000);
    String newLocation;
    UUID newId;
    Showroom newShowroomFromDB;

    @BeforeEach
    public void setUp() {
        Showroom newShowroom = new Showroom(LOCATION);
        showroomRepository.save(newShowroom);
        newLocation = newShowroom.getLocation();
        newId = newShowroom.getId();
        newShowroomFromDB = showroomRepository.findById(newId).get();
    }

    @AfterEach
    public void performPostConditions() {
        if (showroomRepository.findById(newId).isEmpty()) {
            System.out.println("Nothing to delete");
        } else {
            showroomRepository.deleteById(newId);
        }
    }

    @Test
    void saveShowroomTest() {
        Assertions.assertEquals(LOCATION, showroomRepository.findById(newId).get().getLocation());
    }

    @Test
    void getShowroomTest() {
        newShowroomFromDB = showroomRepository.findById(newId).get();
        Assertions.assertEquals(newShowroomFromDB.getId(), newId);
    }

    @Test
    void getListOfShowroomsTest() {
        List<Showroom> showroomList = showroomRepository.findAll();
        assertThat(showroomList.size()).isGreaterThan(0);
    }

    @Test
    void updateShowroomTest() {
        newShowroomFromDB.setLocation(UPDATED_LOCATION);
        Showroom showroomUpdated = showroomRepository.save(newShowroomFromDB);
        assertThat(showroomUpdated.getLocation()).isEqualTo(UPDATED_LOCATION);
    }

    @Test
    void deleteShowroomTest() {
        //showroomRepository.delete(newShowroomFromDB);
        showroomRepository.deleteById(newId);
        Showroom deletedShowroom = showroomRepository.findById(newId).orElse(null);
        Assertions.assertNull(deletedShowroom);
    }
}

