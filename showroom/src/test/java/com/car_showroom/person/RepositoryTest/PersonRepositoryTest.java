package com.car_showroom.person.RepositoryTest;


import com.car_showroom.person.entity.Person;
import com.car_showroom.person.repository.PersonRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
class PersonRepositoryTest {

    @Autowired
    private PersonRepository personRepository;
    private static final String EMAIL = "user" + new Random().nextInt(100) + "@email.com";
    private static final String USERNAME = "Lion" + new Random().nextInt(100);
    private static final String PHONE = "+380505555" + new Random().nextInt(100);
    private static final String UPDATED_PHONE = "+380995555" + new Random().nextInt(100);
    String newEmail;
    String newUsername;
    String newPhone;
    UUID newId;
    Person newPersonFromDB;

    @BeforeEach
    public void setUp() {
        Person newPerson = new Person(EMAIL, USERNAME, PHONE);
        personRepository.save(newPerson);
        newEmail = newPerson.getEmail();
        newUsername = newPerson.getUsername();
        newPhone = newPerson.getPhone();
        newId = newPerson.getId();
        newPersonFromDB = personRepository.findById(newId).get();
    }

    @AfterEach
    public void performPostConditions() {
        if (personRepository.findById(newId).isEmpty()) {
            System.out.println("Nothing to delete");
        } else {
            personRepository.deleteById(newId);
        }
    }

    @Test
    void savePersonTest() {
        Assertions.assertEquals(EMAIL, personRepository.findById(newId).get().getEmail());
        Assertions.assertEquals(USERNAME, personRepository.findById(newId).get().getUsername());
        Assertions.assertEquals(PHONE, personRepository.findById(newId).get().getPhone());
    }

    @Test
    void getPersonTest() {
        newPersonFromDB = personRepository.findById(newId).get();
        Assertions.assertEquals(newPersonFromDB.getId(), newId);
    }

    @Test
    void getListOfPersonTest() {
        List<Person> personList = personRepository.findAll();
        assertThat(personList.size()).isGreaterThan(0);
    }

    @Test
    void updatePersonTest() {
        newPersonFromDB.setPhone(UPDATED_PHONE);
        Person personUpdated = personRepository.save(newPersonFromDB);
        assertThat(personUpdated.getPhone()).isEqualTo(UPDATED_PHONE);
    }

    @Test
    void deletePersonTest() {
        //carRepository.delete(newCarFromDB);
        personRepository.deleteById(newId);
        Person deletedPerson = personRepository.findById(newId).orElse(null);
        Assertions.assertNull(deletedPerson);
    }
}

