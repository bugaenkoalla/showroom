package com.car_showroom.engine;


import com.car_showroom.engine.entity.Engine;
import com.car_showroom.engine.repository.EngineRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Random;
import java.util.UUID;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
class EngineRepositoryTest {

    @Autowired
    private EngineRepository engineRepository;
    private static final String VIN_CODE = "VIN_" + new Random().nextInt(100);
    private static final String CAPACITY = "2.5";
    private static final String UPDATED_CAPACITY = "1.5";
    private static final String POWER = "10WL";
    String newVinCode;
    String newCapacity;
    String newPower;
    UUID newId;
    Engine newEngineFromDB;

    @BeforeEach
    public void setUp() {
        Engine newEngine = new Engine(POWER, CAPACITY, VIN_CODE);
        engineRepository.save(newEngine);
        newVinCode = newEngine.getVinCode();
        newCapacity = newEngine.getCapacity();
        newPower = newEngine.getPower();
        newId = newEngine.getId();
        newEngineFromDB = engineRepository.findById(newId).get();
    }

    @AfterEach
    public void performPostConditions() {
        if (engineRepository.findById(newId).isEmpty()) {
            System.out.println("Nothing to delete");
        } else {
            engineRepository.deleteById(newId);
        }
    }

    @Test
    void saveEngineTest() {
        Assertions.assertEquals(VIN_CODE, engineRepository.findById(newId).get().getVinCode());
        Assertions.assertEquals(CAPACITY, engineRepository.findById(newId).get().getCapacity());
        Assertions.assertEquals(POWER, engineRepository.findById(newId).get().getPower());

    }

    @Test
    void getEngineTest() {
        newEngineFromDB = engineRepository.findById(newId).get();
        Assertions.assertEquals(newEngineFromDB.getId(), newId);
    }

    @Test
    void getListOfEnginesTest() {
        List<Engine> engineList = engineRepository.findAll();
        assertThat(engineList.size()).isGreaterThan(0);
    }

    @Test
    void updateEngineTest() {
        newEngineFromDB.setCapacity(UPDATED_CAPACITY);
        Engine engineUpdated = engineRepository.save(newEngineFromDB);
        assertThat(engineUpdated.getCapacity()).isEqualTo(UPDATED_CAPACITY);
    }

    @Test
    void deleteEngineTest() {
        //engineRepository.delete(newEngineFromDB);
        engineRepository.deleteById(newId);
        Engine deletedEngine = engineRepository.findById(newId).orElse(null);
        Assertions.assertNull(deletedEngine);
    }
}

